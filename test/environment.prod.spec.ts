import { environment } from '../src/environments/environment.prod';

describe('environment.prod', () => {
  it('create', () => {
    expect(environment.production).toBe(true);
  });
});
