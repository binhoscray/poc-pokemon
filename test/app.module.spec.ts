import { TestBed } from '@angular/core/testing';

import { AppModule } from '../src/app/app.module';
import { HttpClientModule } from '@angular/common/http';
import { HttpTestingController } from '@angular/common/http/testing';

describe('AppModule', () => {

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [ AppModule, HttpTestingController ]
        });
    });

    it("should provide 'HttpClientModule' service", () => {
        expect(() => TestBed.get(HttpClientModule)).toBeTruthy();
    });

});