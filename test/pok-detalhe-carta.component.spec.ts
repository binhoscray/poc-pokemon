import { PokDetalheCartaComponent } from '../src/app/modules/pok-detalhe-carta/pok-detalhe-carta.component';

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';


describe('PokDetalheCartaComponent', () => {
  let component: PokDetalheCartaComponent    ;
  let fixture: ComponentFixture<PokDetalheCartaComponent  >;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PokDetalheCartaComponent],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA , NO_ERRORS_SCHEMA ],
      providers: [],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([
          { path: 'listaCartas', redirectTo: '/' }
        ]), TranslateModule.forRoot(),
        TranslateModule.forRoot()
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PokDetalheCartaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('calls getUrlTypeImage()', () => {
    component.getUrlTypeImage('test.png');
  });

  it('calls getUrlTypeImage()', () => {
    component.navigateToCards();
  });

});