import { PokCardComponent } from '../src/app/shared/pok-card/pok-card.component';

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';


describe('PokCardComponent', () => {
  let component: PokCardComponent    ;
  let fixture: ComponentFixture<PokCardComponent  >;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PokCardComponent],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA , NO_ERRORS_SCHEMA ],
      providers: [],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([]),
        TranslateModule.forRoot()
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PokCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('calls getUrlTypeImage()', () => {
    component.getUrlTypeImage('test.png');
  });

  it('calls clickDetails()', () => {
    component.clickDetails();
  });

});