import { PokListaCartasComponent } from '../src/app/modules/pok-lista-cartas/pok-lista-cartas.component';

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { OrderModule } from 'ngx-order-pipe';

import { PokCardModule } from 'src/app/shared/pok-card/pok-card.module';

import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';


describe('PokListaCartasComponent', () => {
  let component: PokListaCartasComponent    ;
  let fixture: ComponentFixture<PokListaCartasComponent  >;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PokListaCartasComponent],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA , NO_ERRORS_SCHEMA ],
      providers: [],
      imports: [
        HttpClientTestingModule,
        OrderModule,
        PokCardModule,
        RouterTestingModule.withRoutes([]),
        TranslateModule.forRoot()
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PokListaCartasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});