import { PokFooterComponent } from '../src/app/components/pok-footer/pok-footer.component';

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('PokFooterComponent', () => {
  let component: PokFooterComponent    ;
  let fixture: ComponentFixture<PokFooterComponent  >;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PokFooterComponent],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA , NO_ERRORS_SCHEMA ],
      providers: [],
      imports: [
        RouterTestingModule.withRoutes([]),
        TranslateModule.forRoot()
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PokFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});