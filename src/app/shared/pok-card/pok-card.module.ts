import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PokCardComponent } from './pok-card.component';

import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatTooltipModule } from '@angular/material/tooltip';

@NgModule({
  declarations: [PokCardComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    MatCardModule,
    MatTooltipModule
  ],
  exports: [PokCardComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class PokCardModule { }
