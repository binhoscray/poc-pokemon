import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pok-card',
  templateUrl: './pok-card.component.html',
  styleUrls: ['./pok-card.component.scss']
})
export class PokCardComponent implements OnInit {

  @Input() card;
  @Output() seeDetailsEvent = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  getUrlTypeImage(type): string {
    return './assets/images/' + type + '.png';
  }

  clickDetails(): void {
    this.seeDetailsEvent.emit(this.card);
  }

}
