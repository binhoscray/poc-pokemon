import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PokDetalheCartaComponent } from './pok-detalhe-carta.component';

import { PokCardModule } from 'src/app/shared/pok-card/pok-card.module';
import { OrderModule } from 'ngx-order-pipe';

import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatTooltipModule } from '@angular/material/tooltip';

@NgModule({
  declarations: [
    PokDetalheCartaComponent
  ],
  imports: [
    CommonModule,
    PokCardModule,
    OrderModule,
    MatCardModule,
    MatTooltipModule,
    MatButtonModule
  ]
})

export class PokDetalheCartaModule { }
