import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PokApiService } from 'src/app/services/pok-api.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-pok-detalhe-carta',
  templateUrl: './pok-detalhe-carta.component.html',
  styleUrls: ['./pok-detalhe-carta.component.scss']
})
export class PokDetalheCartaComponent implements OnInit, OnDestroy {

  card;

  private destroy$: Subject<any> = new Subject();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private pokApiService: PokApiService
  ) { }

  ngOnInit(): void {
    this.getCurrentCard();
  }

  getCurrentCard() {
    this.card = this.pokApiService.getCurrentSelectedDetailCard();
    const id = this.route.snapshot.params.id;

    console.log(id);

    if(!this.card){
      this.pokApiService.getCardById(id).pipe(takeUntil(this.destroy$)).subscribe(
        data =>{
          this.card = data.card;
        },
        error =>{
          console.log(error);
        }
      )
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  getUrlTypeImage(type): string {
    return './assets/images/' + type + '.png';
  }
  navigateToCards():void{
    this.router.navigate(['listaCartas'])
  }

}