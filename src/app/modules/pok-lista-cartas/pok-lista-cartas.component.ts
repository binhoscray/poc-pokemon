import { Component, OnInit, OnDestroy, ViewChild, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { PokApiService } from 'src/app/services/pok-api.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-pok-lista-cartas',
  templateUrl: './pok-lista-cartas.component.html',
  styleUrls: ['./pok-lista-cartas.component.scss']
})
export class PokListaCartasComponent implements OnInit, OnDestroy {

  cards = [];
  filteredCards = [];
  filteredCardsPagination= [];
  searchDebounceTime = 500;
  searchValue: string;

  pageEvent: PageEvent;
  datasource: null;
  pageIndex:number;
  pageSize:number;
  length:number;

  templateRef: TemplateRef<any> | null = null;
  @ViewChild('loading', { static: true })

  loading: TemplateRef<any> | null = null;
  onlineService = false;

  private destroy$: Subject<any> = new Subject();

  constructor(
    private router: Router,
    public pokApiService: PokApiService
  ) { }

  ngOnInit() {
    this.templateRef = this.loading;
    this.getCards();
    this.pageSize = 20;
    this.pageIndex = 0
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
  

  getCards() {
    this.pokApiService.getCards().pipe(takeUntil(this.destroy$)).subscribe(
      data => {
        this.onlineService = true;
        this.cards = data.cards;
        this.filteredCards = this.cards.slice(0,this.pageSize);
        this.length = data.cards.length;
        this.carregamento(data.cards);
      },
      error => {
        console.log(`error`, error);
      }
    );
  }

  carregamento(cards){
    var cont = 0;
    cards.forEach(element => {
      if(cont<this.pageSize){
        this.filteredCardsPagination.push(element);
      }
      cont++;
    });
  }

  paginator(event?:PageEvent){
    var index= 0;
    var quantpaginas = this.cards.length/event.pageSize;
    index = quantpaginas*event.pageIndex;
    event.pageIndex
    this.filteredCards = this.cards.slice(index,index+event.pageSize);
  }

  search(event) {
    if (event) {
      const filtered = this.cards.filter(val => {
        return val.name.toLowerCase().includes(event.toLowerCase())
      });
      this.filteredCards = filtered;
    } else {
      this.filteredCards = this.cards;
    }
  }

  navigateToDetails(event){
    this.pokApiService.updateCurrentSelectedDetailCard(event);
    this.router.navigate(['detalheCarta/' + event.id]);
  }

}
