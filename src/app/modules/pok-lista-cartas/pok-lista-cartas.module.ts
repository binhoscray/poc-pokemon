import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PokListaCartasComponent } from './pok-lista-cartas.component';
import { PokBuscaModule } from 'src/app/shared/pok-busca/pok-busca.module';

import { PokCardModule } from 'src/app/shared/pok-card/pok-card.module';
import { OrderModule } from 'ngx-order-pipe';

import { MatCardModule } from '@angular/material/card';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatPaginatorModule } from '@angular/material/paginator';

@NgModule({
  declarations: [
    PokListaCartasComponent
  ],
  imports: [
    CommonModule,
    PokCardModule,
    PokBuscaModule,
    OrderModule,
    MatCardModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatPaginatorModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class PokListaCartasModule { }
