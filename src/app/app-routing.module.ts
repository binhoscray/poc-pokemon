import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PokListaCartasComponent } from './modules/pok-lista-cartas/pok-lista-cartas.component';
import { PokDetalheCartaComponent } from './modules/pok-detalhe-carta/pok-detalhe-carta.component';


const routes: Routes = [
  {
    path: '',
    component: PokListaCartasComponent
  },
  {
    path: '*',
    redirectTo: 'listaCartas'
  },
  {
    path: 'listaCartas',
    component: PokListaCartasComponent
  },
  {
    path: 'detalheCarta/:id',
    component: PokDetalheCartaComponent
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
