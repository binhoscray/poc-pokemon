import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { PokListaCartasModule } from './modules/pok-lista-cartas/pok-lista-cartas.module';
import { PokDetalheCartaModule } from './modules/pok-detalhe-carta/pok-detalhe-carta.module';

import { PokCardModule } from './shared/pok-card/pok-card.module';
import { PokBuscaModule } from './shared/pok-busca/pok-busca.module';

import { PokHeaderComponent } from './components/pok-header/pok-header.component';
import { PokFooterComponent } from './components/pok-footer/pok-footer.component';

@NgModule({
  declarations: [
    AppComponent,
    PokHeaderComponent,
    PokFooterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    PokListaCartasModule,
    PokCardModule,
    PokBuscaModule,
    PokDetalheCartaModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: httpTranslateLoader,
        deps: [HttpClient]
      }
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }

// AOT compilation support
export function httpTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
