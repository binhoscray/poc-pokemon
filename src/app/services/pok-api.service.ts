import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable} from 'rxjs';
import { Config } from '../app.config';

@Injectable({
  providedIn: 'root'
})

export class PokApiService {

  urlApi = this.config.getApiPokemon();
  private currentSelectedDetailCard;

  constructor(
    private http: HttpClient,
    private config: Config
  ) { }

  getCards(): Observable<any> {
    return this.http.get<any>(this.urlApi + 'cards');
  }

  getCardById(id): Observable<any> {
    return this.http.get(this.urlApi + 'cards/' + id);
  }

  updateCurrentSelectedDetailCard(card?) {
    this.currentSelectedDetailCard = card ? card : null;
  }

  getCurrentSelectedDetailCard() {
    return this.currentSelectedDetailCard;
  }

}
